
# Arc Language Style

## Types

Types are all in PascalCase

## Functions

Functions are in camelCase.

## Variables

Variables are in lower case with words separated by underscores.

## C Macros (for bootstrap)

Macros are functions operating on source text. Treat as functions.
