<meta charset="utf-8">
**The Arc Programming Language**
    Version 0.0

# Introduction

## The Arc Language Design Guidelines

Arc was designed in accordance with a set of guidelines which are enumerated below:
* Apparent Simplicity
* Feature Orthogonality and Composability
* Syntactic Regularity

# Concepts
## Memory Model
## Variables
A variable is a location for storing a value. The set of values that a variable can store depends on its <a href="#toc3.4">type</a>.

A variable declaration or function literal reserves storage for a named variable. From that moment on, any subsequent code may refer to the variable by name to access the value stored there. Despite their name, variables may be marked with a `const` attribute. In that case, it must be initialized with a value where it is declared.

The type of a variable is the type assigned to it when it is declared, or if no type is provided, the type of the expression assigned to it (see section [Variable Declarations] for details).

## Types

# The Language

This section describes the lexis, syntax, and semantics of Arc code.

The language grammar is described with standard Extended Backus-Naur Form (EBNF). For convenience, a table of symbols is provided below.

      Usage      |  Notation
-----------------|---------
 definition      | `=`
 concatenation   | `,`
 end-of-rule     | `;`
 alternation     | `|`
 optional        | `[…]`
 repetition (0+) | `{…}`
 grouping        | `(…)`
 terminal string | `"…"`
 terminal string | `'…'`
 comment         | `(* … *)`
 special         | `?…?`
 exception       | `-`

The form `a … b` is used to indicate a range from `a` to `b` inclusive as alternatives.

## Lexical Structure

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ebnf
codepoint = ? Any Unicode code point ?
character = letter | digit_uni
letter = ? Any codepoint in Unicode category Lu, Ll, Lt, Lm, or Lo ?
digit_uni = ? Any codepoint in Unicode category Nd ?
digit_dec = "0" … "9"
digit_hex = digit | "A" … "F" | "a" … "f"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

### Identifiers

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ebnf
ident       = ident_start {ident_cont}
ident_start = letter
ident_cont  = letter | dec_digit
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Identifiers begin with a character in one of the ranges U+0041 to U+005A ("A" to "Z"), U+0061 to U+007A ("a" to "z"), or U+005F ("_"). Any remaining characters in the identifier must be a valid starting character, or in the range U+0030 to U+0039 ("0" to "9").

Arc is case-sensitive, so `interface` and `Interface` do not refer to the same thing.

Certain identifiers are reserved as keywords and cannot be used for names. They are:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ none
if      else    for     while   return  break
def     let     ref     pure
in      to      as      from
and     or      is      not
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Arc also defines the following non-identifier tokens:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ none
!
+ - * / % ^ & | << >> 
!= = ~ < > <= >=
: :=
, -> => :: . .. ...
( ) [ ] { }
# $
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

### Integer literals

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ebnf
literal_int = literal_dec | literal_hex | literal_bin
literal_dec = digit_dec {digit_dec}
literal_hex = '0' ('x' | 'X') digit_hex {digit_hex}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

An integer literal is a sequence of digits that represent an integer constant. A prefix may be added to indicate a non-decimal base, such as hexadecimal or binary.

Hexadecimal literals use the letters `a-fA-F` to represent values 10 through 15.

### String and character literals

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ebnf
literal_char = "'" (codepoint - "'" | char_escape ) "'"
literal_string = '"' (codepoint - '"' | char_escape) '"'

char_escape = common_escape | unicode_escape
common_escape = '\' ("'" | '"' | '\' | 'n' | 't' | '0')
unicode_escape = '\' 'u' digit_hex {digit_hex}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A character literal represents a single Unicode code point, and is expressed as one or more characters enclosed in single quotes. Within the single quotes, the code point may be represented as a single graphical character, a predefined escape sequence, or by a Unicode value.

### Comments

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ebnf
comment = line_comment | block_comment
block_comment = '/*' {block_comment | character} '*/'
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Comments are used as in-line documentation of a source text, and have two forms:
* Line comments start with the sequence `//` and stop at the end of the line.
* Block comments start with the sequence `/*` and stop at a corresponding `*/` sequence. Block comments may nest inside other block comments.

## Types

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ebnf
type_expr = ident | literal_type
literal_type = literal_struct | type_bool | type_func | type_pointer | type_array | type_slice
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A type defines a set of values, and operations that are associated with the type. A type may be refered by a type name (if it is declared with one), or specified with a type literal.

Certain types are predeclared by the language, and are listed below:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ none
int uint

int8 int16 int32 int64
uint8 uint16 uint32 uint64

bool string
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

All other types must be constructed with type literals.

### The Boolean Type

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ebnf
type_bool = "bool"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The predeclared `bool`ean type represents the binary truth values `true` and `false`.

### Numeric Types

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ none
int    Represents all signed integers representable in the target machine's word size
uint   Represents all unsigned integers representable in the target machine's word size.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Integers are represented with two's complement arithmetic.

-- This means that on x64, (u)int is 64-bits in size, and 32-bits for x32 platforms.

Both numeric types are predeclared in Arc.

### Array Types

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ebnf
type_array = type_expr '[' expr ']'
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

An array is a finite sequence of elements of a single type. The size of an array is called its _length_ and is accessible through its _length_ property. Length is part of an array's type, and must be known at compile time. Every element in an array has an position represented by a `uint`, which can be used to access the element in the array beginning at position 0, up to the length of the array - 1.

Arrays are inherently one-dimensional, but may be composed together to form multi-dimensional arrays.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ arc
int[4]
int[4][4]
int[4][4][4]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

### Pointer Types

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ebnf
type_pointer = '*' type_expr | type_expr '*'
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A pointer type `T*` represents the set of all pointers to variables of a given type `T`, called the base type of the pointer.

### Slice Types

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ebnf
type_slice = type_expr '[' ']'
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A slice is a view over an underlying contiguous array and provides indexed access to the array. The slice type denotes every slice of its element type.

Like arrays, slices have a length, but that length may change during execution. The elements in a slice are refered to in the same way as with arrays, with the first element starting at 0, up to the length of the slice - 1. Slices do not have to start at the beginning of an array, nor do they have to extend to the end of the array.

A slice does not _own_ the array, but instead presents a view that _points to_ the array. As a consequence, a slice may be declared `const`, preventing mutation of the array through the slice, but not preventing mutation through other references to the array.

### Struct Types

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ebnf
type_struct = '[' [ struct_element {',' struct_element} ] ']'
struct_element = [var_container] [ident ':'] type_expression
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A struct is a finite sequence of elements of heterogenous type. Elements in a struct may have a name with which to refer to them. Both named and unnamed elements may be accessed by index, in the same way as with arrays and slices.

### Function Types

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ebnf
type_func = '(' [func_param {',' func_param}] ')' '->' [type_expr] (block | expr)
func_param = [ident ':'] {var_container} type_expression
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A function type encodes the set of all functions that have the same signature.

## Declarations

A declaration binds an identifier to a variable, type, function, or package. Identifiers exist in scopes, which denote the region of source text where the identifier remains bound. Every identifier must be declared before it can be used, and cannot be redeclared from within the same scope. Scopes can be nested within each other, with each inheriting the identifiers declared within all enclosing scopes.

### Scoping Declarations

Arc is lexically scoped using blocks, which are possibly-empty sequences of statements surrounded by braces (`{}`). In addition to explicit blocks in source code, there are a number of implicit scopes:
* The global scope, wherein all other Arc code exists.
* File scope, which encompasses an entire file.
* Module scope, containing everyting in an Arc module.

The rules of scoping are:
* Predeclared identifiers such as `true` and `false` exist in global scope.
* Exported declarations in a module exists in module scope.
* Declarations imported from a file are scoped to the file that imported them.
* Declarations in a file outside of a function exist in file scope.
* The scope of a declaration in a function begins from the end of its declaration to the end of the innermost enclosing block.

### Predeclared Identfiers
A nubmer of identifiers are declared implicitly in global scope. They are:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ebnf
Types: int uint bool string

Constants: true false
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

### Variable Declarations
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ebnf
var_decl = ident (':' type_expr ['=' expr] | ':=' expr)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A variable declaration reserves the space for a variable then binds it to a name, and gives it a type, initial value, or both.

### Type and Value Declarations
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ebnf
def_decl = 'def' ident '=' expr
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Type and value declarations bind an identifier to a type or value.

In the case of Type declarations, a new, distinct type is created with the same structure as its initializer. The new type is called a _defined type_ and is distinct from all other types, including other types that are structurally identical.

## Expressions

Evaluate inside-out, left-to-right, parentheses first

### Arithmetic Operators

TMP:
Standard operators for add, subtract, multiply, divide, modulus, power
"+ - * / % ^"

### Bitwise Operators

TMP:
Standard bitwise operators for and, or, xor, not
"and or xor not"

Operators are overloaded from logical operators. If it's anything but `bool` type, a bitwise operation is used.

### Relational Operators

TMP: Less-than, greater-than, equal, less-equal, greater-equal, not-equal, is, is-not

### Logical Operators

TMP: and, or, not

### Array Operators and Slicing

### Casting Operators

TMP: as, to

As is in-place reinterpretation

To creates a new object

INDEX:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ebnf
expr_index = expr '[' expr ']'
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

SLICE:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ebnf
expr_slice = expr '[' expr '..' expr ']'
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

### Pointers

T*
*T
  equivalent, pointer to T

const *T:
const T*:
  cannot mutate ptr or T

const(T)*: ptr to const T
*const(T): ptr to const T
  can mutate ptr, cannot mutate T

const(*)T:
  cannot mutate ptr, can mutate T

### Calls
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ebnf
call = expr (
              '.' ident
            | '(' [ expr { ',' expr } ] ')'
            )
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Statements

### Blocks

### Variable Declaration

### Assignment

### Control Structures

#### If

#### While

#### For

### Function Calls

### Break

### Return

# Error Handling

# Built-in Functions and Properties

# Program Environment and Execution

## Code Structure and Organization

### Modules

## Source Representation

Arc source files are interpreted as a sequence of Unicode code points encoded in UTF-8. For simplicity, this document will use the term _character_ to refer to a Unicode code point in a source text. Any UTF-8 encoded byte order mark is ignored.

<!-- Markdeep: --><style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src="https://casual-effects.com/markdeep/latest/markdeep.min.js"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script>
