
def module archetype::compiler::syntax::parser

import ::(token, ast, lexer)
import std::data::table::(StaticTable)
import std::io::stdstreams::(stderr)

def Precedence = None
               | Assign
               | Ternary
               | Equality
               | Compare
               | Add
               | Multiply
               | Unary
               | Call
               | Primary

def PrefixParser = (Parser, Token) -> AstNode
def InfixParser = (Parser, AstNode, Token) -> AstNode

def ParseRule = [prefix: PrefixParser, infix: InfixParser, prec: Precedence]

with TokenType def rules = StaticTable(
    [Number,        ParseRule(literal, null, Precedence.None)],
    [Name,          ParseRule(name, null, Precedence.Primary)],
    [LParen,        ParseRule(group, null, Precedence.Call)],
    [Bang,          ParseRule(unary, null, Precedence.Call)],
    [Minus,         ParseRule(unary, binary, Precedence.Add)],
    [Plus,          ParseRule(null, binary, Precedence.Add)],
    [Star,          ParseRule(null, binary, Precedence.Multiply)],
    [Slash,         ParseRule(null, binary, Precedence.Multiply)],
    [Greater,       ParseRule(null, binary, Precedence.Compare)],
    [Less,          ParseRule(null, binary, Precedence.Compare)],
    [LessEqual,     ParseRule(null, binary, Precedence.Compare)],
    [GreaterEqual,  ParseRule(null, binary, Precedence.Compare)],
    [Equal,         ParseRule(null, binary, Precedence.Equality)],
    [BangEqual,     ParseRule(null, binary, Precedence.Equality)],
    defaultValue:   ParseRule(null, null, Precedence.None)
)

def parsePrecedence(parser: Parser, prec: Precedence) = {
    let token = parser.take()
    if token.type not in rules {
        parser.err("%s is not an expression", token)
    }

    let expression = rules[token.type].prefix(parser, token)

    loop while prec <= rules[parser.peek.type].prec {
        token = parser.take()
        expression = rules[token.type].infix(parser, expression, token)
    }

    expression
}

def expression(parser: Parser) -> parser.parsePrecedence(Precedence.Assign)

def name = (p: Parser, t: Token) -> new AstNode(NodeType.Name, t)
def literal = (p: Parser, t: Token) -> new AstNode(NodeType.Literal, t)
def group = (p: Parser, t: Token) -> expression(p) then p.consume(TokenType.RParen)
def unary = (p: Parser, t: Token) -> new AstNode(NodeType.Prefix, t, parsePrecedence(p, Precedence.Unary))
def binary = (p: Parser, left: AstNode, t: Token) {
    let right = parsePrecednece(rules[t.type].prec + 1 as Precedence)
    new AstNode(NodeType.Infix, t, [left, right])
}

def statement(p: Parser) = {
    let token = parser.take()

    match token.type with TokenType {
        Let => let(parser, token)
        default => parser.err("%s is not a statement", token)
    }
}

def let(p: Parser, t: Token) = {
    let name = name(parser, p.take)
    parser.consume(TokenType.Equal)
    let expression = expression(p)
    new AstNode(NodeType.Let, token, [name, expression])
}

def Parser = ref [lexer: Lexer, front: AstNode]
for Parser {
    def new(self, lexer: Lexer) = {
        self.lexer = lexer
        popFront()
    }

    def empty() -> front is null

    def peek() -> lexer.front

    def take -> lexer.front then lexer.popFront

    def consume(type: TokenType) = {
        if type != lexer.front.type {
            err("Type mismatch, expected %s, but got %s instead", type, lexer.front.type)
        }
        else lexer.popFront()
    }

    def popFront() -> if lexer.front.type == TokenType.End { front = null } else { front = statement(this) }

    def err(Args...)(string: String, args: Args) -> stderr.formattedWrite(string, args, end: "\n")
}
