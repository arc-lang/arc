# Setting up Arc Root Compiler

## General Procedure

1. Download and uncompress premake
2. Run premake on arc_root and tools/tester
3. Build arc_root and tools/tester projects
4. Run tools/tester on arc_root

## Platform Specific Instructions

TODO: There is no need yet for this (single developer). When the time comes this is where setup scripts will go. For the moment, just open the vcproj.
