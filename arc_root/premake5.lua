
workspace "arc_bootstrap"
    configurations { "Debug", "Release" }
    platforms { "x32", "x64" }
    language "C++"

    location "generated"

    targetdir "build/bin/%{prj.name}/%{cfg.shortname}"
    objdir "build/obj/%{prj.name}/%{cfg.shortname}"

    -------------------------------
    -- [ GLOBAL PROJECT CONFIG ] --
    -------------------------------
    warnings "Extra"

    flags { "MultiProcessorCompile" }

    filter "configurations:Debug"
        defines { "DEBUG" }
        symbols  "On"

    filter "configurations:Release"
        symbols "Off"
        optimize "Speed"

    filter {}

    
    ------------------------------
    -- [ Project: HighwayHash ] --
    ------------------------------
    project "highwayhash"
        kind "StaticLib"
        files "libraries/highwayhash/**"

        vpaths {
            ["Header Files/*"] = "libraries/highwayhash/highwayhash.h",
            ["Source Files/*"] = "libraries/highwayhash/highwayhash.c",
        }

        function useHighwayHash()
            includedirs "libraries/highwayhash"
            dependson "highwayhash"

            links "highwayhash"
        end

    ----------------------
    -- [ Project: arc ] --
    ----------------------
    project "arc"
        kind "StaticLib"
        files "source/arc/**"

        -- setting up visual studio filters (basically virtual folders).
        vpaths {
            ["Header Files/*"] = { "source/arc/**.h", "source/arc/**.hxx", "source/arc/**.hpp" },
            ["Source Files/*"] = { "source/arc/**.c", "source/arc/**.cxx", "source/arc/**.cpp" },
        }

        useHighwayHash()

        function useArc()
            includedirs "source/arc"
            dependson "arc"

            links "arc"
        end

    -------------------------
    -- [ Project: driver ] --
    -------------------------
    project "driver"
        kind "ConsoleApp"
        files "source/driver/**"

        -- setting up visual studio filters (basically virtual folders).
        vpaths {
            ["Header Files/*"] = { "source/driver/**.h", "source/driver/**.hxx", "source/driver/**.hpp" },
            ["Source Files/*"] = { "source/driver/**.c", "source/driver/**.cxx", "source/driver/**.cpp" },
        }

        useArc()

    ------------------------
    -- [ Project: tests ] --
    ------------------------
    project "tests"
        kind "ConsoleApp"
        files "source/tests/**"

        includedirs "libraries/catch/"

        -- setting up visual studio filters (basically virtual folders).
        vpaths {
            ["Header Files/*"] = { "source/tests/**.h", "source/tests/**.hxx", "source/tests/**.hpp" },
            ["Source Files/*"] = { "source/tests/**.c", "source/tests/**.cxx", "source/tests/**.cpp" },
        }

        useArc()

