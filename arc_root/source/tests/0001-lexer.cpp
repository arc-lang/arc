#include "catch.hpp"

#include "common.h"
#include "parser.hpp"
#include "source.hpp"
#include "token.hpp"
#include <string>

using namespace arc;

SCENARIO("Scanning empty source text", "[lexer]") {
    GIVEN("NULL as the source string") {
        auto scanner = Scanner(Source{ NULL, NULL });
        
		THEN("The lexer must report being empty") {
			REQUIRE(scanner.isEmpty());
		}
    }

    GIVEN("An empty source string") {
        auto scanner = Scanner(Source{ NULL, "\0" });

		THEN("The lexer must report being empty") {
			REQUIRE(scanner.isEmpty());
		}
    }
}

SCENARIO("Scanning operators", "[lexer][!mayfail]") {
    // GIVEN("broken operator") {
    //     THEN("The lexer must correctly identify the token, then report empty.") {
    //         auto id = TOKEN_BANG_EQUAL;
    //         auto prefix = GENERATE(as<std::string>(), "", " ");
    //         auto infix = asText(id);
    //         auto suffix = GENERATE(as<std::string>(), "", " ");

    //         auto final = prefix + infix + suffix;

    //         auto scanner = Scanner(Source{ NULL, final.c_str() });
    //         REQUIRE(!scanner.isEmpty());
    //         REQUIRE(scanner.scanToken().type == id);
    //         CAPTURE(final);
    //         CAPTURE(asText(id));
    //         REQUIRE(scanner.scanToken().type == TOKEN_EOF);
    //         REQUIRE(scanner.isEmpty());
    //     }
    // }

    GIVEN("An operator surrounded by whitespace") {
        THEN("The lexer must correctly identify the token, then report empty.") {
            auto id = (TokenType) GENERATE(range((int) TOKEN_HASH, (int) TOKEN_FAT_ARROW + 1));
            auto prefix = GENERATE(as<std::string>(), "", " ");
            auto infix = asText(id);
            auto suffix = GENERATE(as<std::string>(), "", " ");

            auto final = prefix + infix + suffix;

            auto scanner = Scanner(Source{ NULL, final.c_str() });
            REQUIRE(!scanner.isEmpty());
            REQUIRE(scanner.scanToken().type == id);
			CAPTURE(final);
			CAPTURE(asText(id));
            REQUIRE(scanner.scanToken().type == TOKEN_EOF);
			REQUIRE(scanner.isEmpty());
        }
    }
}
