#pragma once

#include "common.h"

namespace arc {
// Owns and manages the memory of a string
class arc_string {
    char const * _ptr;
    size_t _length;

public:
    arc_string();

    arc_string(char c);

    arc_string(char const * str);

    ~arc_string();

    char const * ptr() const;

    size_t length() const;

    char operator[](size_t i) const;

    bool operator=(const arc_string& rhs);

    hash_t hash();
};

}
