#include "token.hpp"

#include "common.h"

namespace arc {

char const * token_text[NUM_TOKENS] = {
    "#token_eof", 
    "#token_invalid",

    // Single-character tokens
    "#",
    "(", 
    ")",
    "{", 
    "}",
    "[", 
    "]",
    ",", 
    ".", 
    ":", 
    ";",
    "+", 
    "-", 
    "/", 
    "*", 
    "^",
    "&", 
    "|", 
    "%",
    "!", 
    "=", 
    ">", 
    "<",

    // Two-character tokens
    "!=", 
    ">=", 
    "<=",
    ":=",
    "<<", 
    ">>",
    "->", 
    "=>",


    // Literals
    "#token_name", 
    "#token_string", 
    "#token_integer", 
    "#token_char",


    // Keywords
    "if", 
    "else", 
    "for", 
    "while", 
    "return", 
    "break",
    "def", 
    "let", 
    "ref",
    "in", 
    "to", 
    "as", 
    "from",
    "and", 
    "or", 
    "is", 
    "not",
};

char const * asText(TokenType type) {
    assert(type <= NUM_TOKENS);

    return token_text[type];
}

}
