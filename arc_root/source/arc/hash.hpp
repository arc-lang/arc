#pragma once

#include "common.h"

namespace arc {

class string;
hash_t hashString(const string& str);

}
