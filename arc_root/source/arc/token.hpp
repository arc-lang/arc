#pragma once

#include "common.h"

namespace arc {

enum TokenType {
    TOKEN_EOF, TOKEN_INVALID,

	// Single-character tokens
	TOKEN_HASH,
	TOKEN_LPAREN, TOKEN_RPAREN,
	TOKEN_LBRACE, TOKEN_RBRACE,
	TOKEN_LBRACKET, TOKEN_RBRACKET,
	TOKEN_COMMA, TOKEN_DOT, TOKEN_COLON, TOKEN_SEMICOLON,
	TOKEN_PLUS, TOKEN_MINUS, TOKEN_SLASH, TOKEN_STAR, TOKEN_CARET,
	TOKEN_AND, TOKEN_PIPE, TOKEN_PERCENT,
	TOKEN_BANG, TOKEN_EQUAL, TOKEN_GREATER, TOKEN_LESS,

	// Two-character tokens
	TOKEN_BANG_EQUAL, TOKEN_GREATER_EQUAL, TOKEN_LESS_EQUAL,
	TOKEN_COLON_EQUAL,
	TOKEN_LSHIFT, TOKEN_RSHIFT,
	TOKEN_ARROW, TOKEN_FAT_ARROW,

	// Literals
	TOKEN_NAME, TOKEN_STRING, TOKEN_INTEGER, TOKEN_CHAR,

	// Keywords
	KEY_IF, KEY_ELSE, KEY_FOR, KEY_WHILE, KEY_RETURN, KEY_BREAK,
	KEY_DEF, KEY_LET, KEY_REF,
	KEY_IN, KEY_TO, KEY_AS, KEY_FROM,
	KEY_AND, KEY_OR, KEY_IS, KEY_NOT,
	
	// !!! MAKE SURE THIS IS THE LAST MEMBER !!!
	NUM_TOKENS
};

char const * asText(TokenType);

struct Token {
    TokenType type;
    size_t location;
    size_t length;
};

}
