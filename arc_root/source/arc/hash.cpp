#include "common.h"
#include "hash.hpp"
#include "highwayhash.h"
#include "arc_string.h"

namespace arc {

const uint64_t HASH_KEY[4] = {
    19027847019256, 856320186026426, 28190927301, 184620826491
};

class arc_string;
hash_t hashString(const arc_string& str) {
    HighwayHashCat state;

    HighwayHashCatStart(HASH_KEY, &state);
    HighwayHashCatAppend((const uint8_t*) str.ptr(), str.length(), &state);
    hash_t hash = HighwayHashCatFinish64(&state);

    return hash;
}

}
