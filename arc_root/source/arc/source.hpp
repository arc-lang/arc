#pragma once

#include "common.h"

namespace arc {

struct Source {
	char const * const name;
    
    // pointer to constant chars
	char const * const text;
};

struct SourceLoc {
	Source source_info;
	size_t offset;
};

}
