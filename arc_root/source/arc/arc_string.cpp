#include "common.h"
#include "arc_string.h"
#include "hash.hpp"

namespace arc {

arc_string::arc_string() {

}

arc_string::arc_string(char c) {

}

arc_string::arc_string(char const * str) {

}

arc_string::~arc_string() {

}

char const * arc_string::ptr() const {
    return _ptr;
}

size_t arc_string::length() const {
    return _length;
}

char arc_string::operator[](size_t i) const {
    return '\0';
}

bool arc_string::operator=(const arc_string& rhs) {
    return false;
}

hash_t arc_string::hash() {
    return 0;
}

}
