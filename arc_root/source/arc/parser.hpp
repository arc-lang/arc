#pragma once

namespace arc {

struct Token;
struct Source;

/*
Takes a source file, and breaks it up into tokens.
*/
struct Scanner {
	char const * const name;

    // Pointer to the current character
	// We store a pointer because it's just a little faster
	char const * current;

    // The first character in the text to be scanned
    char const * const front;

    Scanner(Source source);

    // Returns true if the scanner has reached the end of the source text
    bool isEmpty() const;

    // Returns the location of the scanner in the source
    size_t location() const;

    // Returns the current character then advances the scanner by 1
    char advance();

    // Peeks at the current character without advancing the scanner
    char peekChar() const;

    Token scanToken();

private:
	bool is_empty;
};

}
