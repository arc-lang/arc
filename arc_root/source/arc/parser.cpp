
#include "common.h"
#include "parser.hpp"
#include "token.hpp"
#include "source.hpp"

namespace arc {

Scanner::Scanner(Source source): name(source.name), front(source.text) {
	current = source.text;
	is_empty = (current == NULL || *current == '\0') ? true : false;
}

bool Scanner::isEmpty() const { return is_empty; }

size_t Scanner::location() const { return (size_t) (current - front); }

char Scanner::peekChar() const  { return *current; }

char Scanner::advance() {
    auto c = *current;
    current++;
	if (*current == '\0') is_empty = true;
    return c;
}

void consumeWhitespace(Scanner* scanner);
Token makeToken(Scanner* scanner, TokenType type, size_t location);
Token makeToken2(Scanner* scanner, TokenType type, size_t location);
bool isDigit(char c);
bool isAlpha(char c);
bool isIdCont(char c);

Token Scanner::scanToken() {
    consumeWhitespace(this);

    if (isEmpty())
        // We use current: char* - front: char* to get the length & final
        // position of the source text
        return Token{TOKEN_EOF, location(), 0};

    const size_t loc = location();
    const char c = advance();

    switch (c) {
    case '#': return makeToken(this, TOKEN_HASH, loc);
    case '(': return makeToken(this, TOKEN_LPAREN, loc);
    case ')': return makeToken(this, TOKEN_RPAREN, loc);
    case '{': return makeToken(this, TOKEN_LBRACE, loc);
    case '}': return makeToken(this, TOKEN_RBRACE, loc);
    case '[': return makeToken(this, TOKEN_LBRACKET, loc);
    case ']': return makeToken(this, TOKEN_RBRACKET, loc);
    case ',': return makeToken(this, TOKEN_COMMA, loc);
    case '.': return makeToken(this, TOKEN_DOT, loc);
    // TOKEN_SEMICOLON parsed below
    case ';': return makeToken(this, TOKEN_SEMICOLON, loc);

    case '+': return makeToken(this, TOKEN_PLUS, loc);
    // TOKEN_MINUS parsed below
    case '/': return makeToken(this, TOKEN_SLASH, loc);
    case '*': return makeToken(this, TOKEN_STAR, loc);
    case '^': return makeToken(this, TOKEN_CARET, loc);
    case '&': return makeToken(this, TOKEN_AND, loc);
    case '|': return makeToken(this, TOKEN_PIPE, loc);
    case '%': return makeToken(this, TOKEN_PERCENT, loc);
    
    case '!':
        if (*current == '=')
            return makeToken2(this, TOKEN_BANG_EQUAL, loc);
        return makeToken(this, TOKEN_BANG, loc);
    case '-':
        if (*current == '>')
            return makeToken2(this, TOKEN_ARROW, loc);
        return makeToken(this, TOKEN_MINUS, loc);
    case ':':
        if (*current == '=')
            return makeToken2(this, TOKEN_COLON_EQUAL, loc);
        return makeToken(this, TOKEN_COLON, loc);
    case '<':
        if (*current == '=')
            return makeToken2(this, TOKEN_LESS_EQUAL, loc);
        if (*current == '<')
            return makeToken2(this, TOKEN_LSHIFT, loc);
        return makeToken(this, TOKEN_LESS, loc);
    case '>':
        if (*current == '=')
            return makeToken2(this, TOKEN_GREATER_EQUAL, loc);
        if (*current == '>')
            return makeToken2(this, TOKEN_RSHIFT, loc);
        return makeToken(this, TOKEN_GREATER, loc);
    case '=':
        if (*current == '>')
            return makeToken2(this, TOKEN_FAT_ARROW, loc);
        return makeToken(this, TOKEN_EQUAL, loc);

    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
        while (!isEmpty() && isDigit(*current)) {
            advance();
        }
		return Token{ TOKEN_INTEGER, loc, location() - loc };

    default:
        if (isAlpha(c)) {
            while (!isEmpty() && isIdCont(*current)) advance();

            // Unfinished: we don't handle string interning or keyword checking yet.
            const size_t length = location() - loc;
			return Token{ TOKEN_NAME, loc, length };
        }
    }
	
	assert(false);
	return Token{ TOKEN_NAME, loc, 0 };
}

// Consumes \n \r \t ' ' \f
void consumeWhitespace(Scanner* scanner) {
    while (!scanner->isEmpty()) {
        const char c = scanner->peekChar();
        const bool is_white = (c == '\n') ||
							  (c == '\r') ||
							  (c == '\t') ||
							  (c == '\f') ||
							  (c == ' ');

        if (!is_white) return;
        scanner->advance();
    }
}

Token makeToken(Scanner* self, TokenType type, size_t location) {
    return Token{ type, location, 1 };
}

Token makeToken2(Scanner* self, TokenType type, size_t location) {
	self->advance();
    return Token{ type, location, 2 };
}

bool isDigit(char c) { return '0' <= c && c <= '9'; }

bool isAlpha(char c) { return ('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z'); }

bool isIdCont(char c) { return isAlpha(c) || c == '_'; }

}
