#pragma once

#include <assert.h>

#include <cstdint>

/* NULL, offsetof, size_t, ptrdiff_t, nullptr_t, max_align_t, byte */
#include <cstddef>

namespace arc {

typedef intmax_t int_t;
typedef uintmax_t uint_t;
typedef uint64_t hash_t;

}
