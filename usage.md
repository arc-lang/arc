
# Setting up the compiler


## Bootstrapping

Assumptions:
1. A system compiler is available.

Procedure:
1. (optional) build and run setup tool at `arc_root/tools/setup`. A number of other tools will be compiled, and the downloaded bootstrap source will be compiled and verified.
2. run `arc_root/tools/bootstrap` to compile the current version of the compiler.
